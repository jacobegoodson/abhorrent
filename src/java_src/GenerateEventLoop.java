package java_src;

import clojure.lang.IFn;
import javafx.animation.*;
import javafx.util.Duration;


public class GenerateEventLoop {
    public static Timeline eventLoop(IFn function){
        final Duration delta = Duration.millis(1000/60d);
        final KeyFrame oneFrame = new KeyFrame(
                delta,
                function::invoke);
        final Timeline loop = new Timeline(oneFrame);
        loop.setCycleCount(Animation.INDEFINITE);
        return loop;
    }
}
