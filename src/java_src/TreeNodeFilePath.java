package java_src;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TreeItem;


public class TreeNodeFilePath extends TreeItem<String> {
    private final StringProperty path = new SimpleStringProperty();

    public String getPath() {
        return path.get();
    }

    public StringProperty pathProperty() {
        return path;
    }


    public void setPath(String value) {
        this.path.set(value);
    }
    //public String getPath(String value) {
    //    return this.path.get();
    //}
//
    //public final StringProperty pathProperty() {
        //return this.wrapText;
   // }
}
