package java_src;

public class MutableData {
    private Object data;

    public MutableData(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public Object setData(Object data) {
        this.data = data;
        return data;
    }
}
