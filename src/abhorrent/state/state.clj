(ns abhorrent.state.state
  (:require [cljfx.api :as fx])
  (:import (javafx.scene.robot Robot)))

(def robot)
(fx/on-fx-thread (def robot (Robot.)))

(defmulti event-handler :event/type)

(def code-area-context (atom ""))

(def state
  (atom {:code-area-text ""
         :current-project nil
         :ide-windows-viewable false
         :file-tree-view []
         :file-tree []
         :multi-methods []
         :traits []
         :key-typed-event nil
         :current-leaf-location ""
         :trait-context-menus []
         :creation-window-props {:show-creation-window false
                                 :mouse-x 0
                                 :mouse-y 0}}
        :path ""))
