(ns abhorrent.events.project-overview
  (:use [abhorrent.state.state])
  (:require [clojure.java.io :as io]
            [abhorrent.watch-directories :as watch]))

(defmethod event-handler :project-selection [{:keys [option]}]
  (let [base (io/file (watch/base-object-location option))
        base-parent (.getParentFile base)
        project-files (map (fn [x] (.getName x))
                           (watch/children base-parent))]
    (if-not (.exists base-parent)
      (io/make-parents (str base "\\Filler")))
    (if-not (some (fn [x] (= x "Traits")) project-files)
      (io/make-parents (str base-parent "\\Traits\\Filler")))
    (if-not (some (fn [x] (= x "MultiMethods")) project-files)
      (io/make-parents (str base-parent "\\MultiMethods\\Filler"))))
  (swap! state assoc :current-project option :ide-windows-viewable true))
