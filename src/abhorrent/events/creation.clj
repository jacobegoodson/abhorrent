(ns abhorrent.events.creation
  (:use [abhorrent.state.state])
  (:require [abhorrent.watch-directories :as watch]))

; creates basic source code for a trait
(defn- gen-trait-source
  [name]
  (str "(deftrait "
       (apply str (map #(str % "\n")
                       [name
                        "          (uses )"
                        "          (class-slots )"
                        "          (instance-slots ))"]))))

;; hide the creation window if focus is lost
(defmethod event-handler :creation-window-mouse-exit [{:keys [fx/event]}]
  (swap! state assoc :creation-window-props {:mouse-x 0
                                             :mouse-y 0
                                             :show-creation-window false}))

; make sure the text field is empty, create the trait with some template code, close the creation window
(defmethod event-handler :text-field-submitted [{:keys [fx/event]}]
  (let [target-dir (watch/get-trait-directory)
        obj (.getSource event)
        name (.getText (.getSource event))]
    (.setText obj "")
    (spit (str target-dir "\\" name ".trait") (gen-trait-source name))
    (swap! state assoc :creation-window-props {:mouse-x 0
                                               :mouse-y 0
                                               :show-creation-window false})))


