(ns abhorrent.events.traits
  (:use [abhorrent.state.state])
  (:require [abhorrent.watch-directories :as watch]
            [abhorrent.internal-state-handler-continuations :as co]
            [clojure.java.io :as io]
            [parinferish.core :as pinfer])
  (:import (org.fxmisc.richtext CodeArea)
           (javafx.scene.input KeyCode)))

(defn- trait-selected?
  [path]
  (not= (last (clojure.string/split path #"\\")) "Traits"))

(defn- save-code-changes!
  [event-name path event]
  (if (= "KEY_PRESSED" event-name)
      (if (trait-selected? path)
          (if (.isControlDown event)
              (if (= KeyCode/ENTER (.getCode event))
                  (spit (@state :path) (.getText (watch/get-trait-code-area))))))))

;; TODO write a parser that will validate the code before saving it!
;; TODO write the code to all classes in the skookum hierarchy
;; TODO add parinfer powers to code!
;; TODO test if a nested map rehooks event
;; TODO ignore parinfer for now, may just use js version! alternatively check cross parinfer

(defmethod event-handler :trait-intercept-typing [{:keys [fx/event]}]
  (let [event-name (.getName (.getEventType event))
        path (@state :path)]
    (println event-name)
    (save-code-changes! event-name path event)
    (if (= "KEY_TYPED" event-name)
      (let [event-char (.getCharacter event)
            code-area (watch/get-trait-code-area)
            cursor-line (.getCurrentParagraph code-area)
            cursor-col (.getCaretColumn code-area)
            former-caret-pos (.getCaretPosition code-area)
            format-text (pinfer/parse (.getText code-area) {:mode :indent :cursor-line cursor-line :cursor-column cursor-col})]
        (if (not (empty? (filter #(= % event-char) co/consume-keys)))
          (do (.consume event)
              (swap! state assoc :key-typed-event event)))
        (println (pinfer/diff format-text))))))

(defmethod event-handler :trait-text-changed [{:keys []}])



(defn- creation-context-menu
  [path]
  {:fx/type :menu-item
   :text "Create Trait"
   :on-action {:event/type :trait-create-taxonomy
               :path path}})

(defn- deletion-context-menu
  [path]
  {:fx/type :menu-item
   :text "Delete Trait"
   :on-action {:event/type :trait-delete-selected
               :path path}})
;; TODO you are here, traits are now pasting code in trait code area
(defmethod event-handler :trait-selection [{:keys [fx/event]}]
  (let [path (.getPath event)
        last (last (clojure.string/split path #"\\"))
        ^CodeArea code-area (watch/get-trait-code-area)]
    (.clear code-area)
    (if-not (.isDirectory (io/file path))
            (.insertText code-area (.getCaretPosition code-area) (slurp path)))
    (if (= last "Traits")
        (swap! state
               assoc
               :trait-context-menus
               [(creation-context-menu path)]
               :path path)
        (swap! state
               assoc
               :trait-context-menus
               [(deletion-context-menu path)]
               :path path))))

(defmethod event-handler :trait-delete-selected [{:keys [fx/event path]}]
  (.delete (io/file path)))

(defmethod event-handler :trait-create-taxonomy [{:keys [fx/event path]}]
  (swap! state assoc :creation-window-props {:mouse-x (.getMouseX robot)
                                             :mouse-y (.getMouseY robot)
                                             :show-creation-window true}))

(defmethod event-handler :trait-clicked [{:keys [fx/event path]}]
  (try
    (if (= (.getText (.getTarget event)) "Traits")
        (swap! state
             assoc
             :trait-context-menus
             [(creation-context-menu path)]))
    (catch Exception e)))
