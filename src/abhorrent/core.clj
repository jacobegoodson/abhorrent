(ns abhorrent.core
  (:require [cljfx.api :as fx]
            [abhorrent.functional-javafx-wrappers :as ca]
            [abhorrent.internal-state-handler-continuations :as cont]
            [abhorrent.watch-directories :as watch]
            [clojure.java.io :as io]
            [abhorrent.windows.project-overview :as project-overview-window]
            [abhorrent.windows.traits :as traits-window]
            [abhorrent.windows.multi-methods :as multi-methods-window]
            [abhorrent.windows.creation :as creation-window])
  (:import (org.fxmisc.richtext CodeArea)
           (javafx.event Event)
           (javafx.scene.input KeyCode)
           (java_src GenerateEventLoop))
  (:use [abhorrent.state.state]
        [abhorrent.events.multi-methods]
        [abhorrent.events.project-overview]
        [abhorrent.events.traits]
        [abhorrent.events.creation]))


(defmethod event-handler :typing [{:keys [fx/event]}]
  (println (.getCaretPosition ^CodeArea (.getSource ^Event event))))

;; TODO look at more cljfx examples, use caretDisplacement to see if you can move the caret around
(def consume-keys
  (clojure.string/split "qwertyuiopasdfghjklzxcvbnm -" #""))


;; key commands comment
(defmethod event-handler :intercept-typing [{:keys [fx/event]}]
  (let [event-name (.getName (.getEventType event))
        code-area-context @code-area-context]
    (if (= "KEY_TYPED" event-name)
      (let [event-char (.getCharacter event)]
      (if (not (empty? (filter #(= % event-char) consume-keys)))
          (do (.consume event)
              (swap! state assoc :key-typed-event event)))))))



;; TODO when pressing a tree item it needs to extract the text and place it in
;; TODO the code area after parsing it into skive/abhorent

;;TODO work on traits first they will be easier and you can make design choices from their

(comment (if (.isControlDown event)
           (if (= KeyCode/ENTER (.getCode event))
             (if (= "Traits" code-area-context)
               (let [text (.getText obj)
                     data (read-string text)
                     ;;TODO parse before writing to file to ensure correctness
                     name (apply str (butlast (rest (str (second data)))))
                     location (:location (watch/get-traits state))]
                 (spit (str location "\\" name ".trait") text)))))


         ;(println {id (.getCaretPosition obj)}))
         (cond (= "KEY_TYPED" event-name)
               (if (not (empty? (filter #(= % (.getCharacter event)) consume-keys)))
                 (do (.consume event)
                     (let [[key-chorded] (cont/run cont/key-chord event)]
                       (if (not= :awaiting-key-chord (second key-chorded))
                         (ca/insert-text-at-caret obj key-chorded)))))))

(defmethod event-handler :delete-selected [{:keys [fx/event current-leaf-location]}]
  (let [dirs (clojure.string/split current-leaf-location #"\\")
        last (last dirs)]
    ; do not delete any of the main project dirs
    (if-not (or (= last "Traits") (= last "MultiMethods") (some (fn [x] (= x "Object")) dirs))
      (io/delete-file (io/file current-leaf-location)))))

;; TODO treeitem selection is no longer single window only, refactor
(defmethod event-handler :tree-item-selected [{:keys [fx/event path]}]
  (let [code-area ^CodeArea (watch/get-code-area)
        leaf (.getValue event)]
    (swap! state assoc :current-leaf-location (.getPath event))
    (reset! code-area-context leaf)
    (cond (= leaf "MultiMethods")
          (.replaceText code-area
                        (str "(defmulti name\n"
                             "          (dispatches-on )\n"
                             "          (arguments )\n"
                             "          (body ))"))
          (= leaf "Traits")
          (if (empty? (:content (watch/get-traits state)))
            (do (.replaceText code-area (str "(deftrait *name*\n"
                                             "          (with )\n"
                                             "          (class-slots )\n"
                                             "          (instance-slots ))")))))))










(defmethod event-handler :text-changed [{:keys [fx/event project-toggle-group]}]
  (println event))
;; TODO uncomment keychords, add one for each code area one in traits one in multimethods
(comment (def pump-events (GenerateEventLoop/eventLoop
                            (fn [e]
                              (let [code-area ^CodeArea (watch/get-code-area)
                                    event (:key-typed-event @state)]
                                (if event
                                  (cont/run cont/key-chord state event code-area))))))
         (.play pump-events))


;; TODO multiple window setup is starting to work, get traits working, remove code area from main window, may not need context anymore!
(def renderer
  (fx/create-renderer
    :middleware (fx/wrap-map-desc (fn [state]
                                    {:fx/type fx/ext-many
                                     :desc [{:fx/type project-overview-window/project-overview-window
                                             :state   state}
                                            {:fx/type traits-window/traits-window
                                             :state   state}
                                            {:fx/type multi-methods-window/multi-methods-window
                                             :state   state}
                                            {:fx/type creation-window/creation-window
                                             :state   state}]}))
    :opts {:fx.opt/map-event-handler event-handler}))

(fx/mount-renderer state renderer)



