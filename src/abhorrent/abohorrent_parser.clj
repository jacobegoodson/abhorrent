(ns abhorrent.abohorrent-parser
  (:require [instaparse.core :as insta]
            [clojure.java.io :as io]
            [watch.man :as watch])
  (:use [abhorrent.hangman]
        [clojure.data])
  (:import (java.nio.file Files Paths FileVisitOption)
           (java.net URI)
           (java.io File)
           (java.util Arrays)
           (java_src DirsWatcher)))


;; CODEAREA START
;; start parsing abhorrent
;; TODO automatic templating everytime a class is created
;; TODO should classes only be created through unreal/(insert game engine) here engine
;; TODO handle parse errors with these commands
;; (:index :reason :line :column :text)
;; (insta/get-failure parse-something) => nil or error record

(defmacro defrule
  [name rule]
  `(def ~(symbol (str name "-rule")) (str '~name " = " ~(clojure.string/replace rule "\n" ""))))
(defmacro defrule-omit
  [name rule]
  `(def ~(symbol (str name "-rule")) (str "<" '~name "> = " ~(clojure.string/replace rule "\n" ""))))

(defn compose-rules
  [rules]
  (apply str (interpose " " rules)))

(defmacro defrule-compose
  [name rules]
  `(def ~name ~(compose-rules (eval rules))))

(defmacro defparser
  [name rules]
  `(def ~name (insta/parser ~(compose-rules (eval rules)) :auto-whitespace :standard)))


;; state!
(def abhorrent-classes (atom {}))
(def skookum-classes (atom {}))




(defrule class-slots
         "open-paren
         <'class-slots '>
         slots
         close-paren")

(defrule extends
         "open-paren <'extends '> class-name* close-paren")

(defrule instance-slots
         "
         open-paren
         <'instance-slots '>
         slots
         close-paren
         ")

(defrule slots
         "slot*")
(defrule slot
         "
         open-paren
         word
         atom
         close-paren
         ")

(defrule with "open-paren <'with '> (taxonomy-name | class-name)*  close-paren")

(defrule class-name "#'[a-z]+\\!'")
(defrule taxonomy-name "#'@[a-z]+'")
(defrule-omit atom "float | integer")
(defrule word "#'[a-z\\-]+'")
(defrule integer "#'([0-9]|[1-9][0-9]*)'")
(defrule float "integer '.' integer")
(defrule-omit open-paren "<'('>")
(defrule-omit close-paren "<')'>")

(defrule-compose basic-rules
                 [word-rule
                  taxonomy-name-rule
                  class-name-rule
                  class-slots-rule
                  instance-slots-rule
                  slots-rule
                  slot-rule
                  atom-rule
                  integer-rule
                  float-rule
                  open-paren-rule
                  close-paren-rule])

;; class parser definition start
(defrule class-definition
         "
         open-paren
         <'defclass '>
         class-name
         extends
         with
         class-slots
         instance-slots
         close-paren
         ")

(defparser class-parser
           [class-definition-rule
            extends-rule
            with-rule
            basic-rules])

;; class parser definition end

;; test for class parser
(comment
  (insta/parse class-parser
               "(defclass kek!
                          (extends )
                          (with )
                          (class-slots )
                          (instance-slots ))"))

;; taxonomy definition start
(defrule taxonomy
         "open-paren <'taxonomy '> class-name* close-paren")

(defparser taxonomy-parser
           [taxonomy-rule
            basic-rules])

;; taxonomy definition end

;; test for taxonomy parser
(comment
  (insta/parse taxonomy-parser "(deftaxonomy @hi)")
  (insta/parse taxonomy-parser "(deftaxonomy @hi there! @kek)"))

;; trait parser definition begin

(defrule trait
         "
         open-paren
           <'deftrait '>
           word
           class-slots
           instance-slots
           parameters
           body
         close-paren
         ")
(defrule parameters
         "
         open-paren
         <'parameters '>
           (open-paren
             word
             atom
           close-paren)*
         close-paren
         ")
;;
(defrule body
         "
         open-paren
           <'body '>
           atom*
         close-paren
         ")

(defparser trait-parser
           [trait-rule
            parameters-rule
            body-rule
            basic-rules])

;; trait parser definition end
;; trait parser test
(comment (insta/parse trait-parser "(deftrait kek-dat (class-slots ) (instance-slots ) (parameters ) (body 1 2 3))"))


;; start of transformers


(defn extract-symbols-transform
  [source]
  (insta/transform {:extends (fn [[_ x]] [:extends x])
                    :slot (fn [[_ name] _] name)
                    :slots (fn [& slots] (into [] slots))
                    :with (fn [& syms] (conj [:with] (mapv (fn [[_ sym]] sym) syms)))}
                   (insta/parse class-parser
                                source)))

(defn check-dup-symbols
  [source]
  (let [source-ast (rest (extract-symbols-transform source))
        [[_ class-name]] source-ast
        [_ [_ extends]] source-ast
        [_ _ [_ with]] source-ast
        [_ _ _ [_ class-slots]] source-ast
        [_ _ _ _ [_ instance-slots]] source-ast]
    (cond (= class-name extends) {:error "cannot extend from class with the same name!"}
          (some #(= class-name %) with) {:error "class name was found in with"}
          (some #(= extends %) with) {:error "extend name was found in with"}
          (let [joined (concat class-slots instance-slots)
                distinct (distinct joined)]
            (not= (count joined) (count distinct)))
          {:error "duplicate values found in slots"})))

;; test for dup symbols
(comment (check-dup-symbols
           "(defclass kek!
             (extends lawl!)
             (with lawl! @kek)
             (class-slots (x 1) (z 2))
             (instance-slots (y 2) (a 2)))"))

(defn translate-class-name
  [name]
  (apply str (butlast (flatten (map (fn [x] (concat
                                              (clojure.string/upper-case (first x))
                                              (rest x)))
                                    (clojure.string/split name #"-"))))))

(do
  (def class-definition-transform
    {:slot (fn [name val] [name val])
     :word identity
     :integer identity
     :slots (fn [& slots] slots)
     :class-slots (fn [slots] [:class-slots (into {} slots)])
     :class-name identity
     :taxonomy-name identity
     :extends (fn [name] [:extends name])
     :class-definition (fn [name & rest] [name (into {} rest)])
     :with (fn [& names] [:with (into #{} names)])
     :instance-slots (fn [slots] [:instance-slots (into {} slots)])})
  (let [[class data] (insta/transform class-definition-transform
                                      (insta/parse class-parser
                                                   "(defclass kek!
                                 (extends lawl!)
                                 (with lawl! @kek)
                                 (class-slots (x 1) (z 2))
                                 (instance-slots (z 2) (v 2)))"))]
    (swap! abhorrent-classes assoc class data)
    (translate-class-name class)))

;; TODO figure out skookum transformation!
(remove-watch abhorrent-classes :class-change)
(add-watch abhorrent-classes :class-change (fn [key atom old-value new-value]
                                             (let [[diffed? _ _] (diff old-value new-value)]
                                               (if (or diffed?)
                                                 (println (let [[key value] (keys new-value)] key))))))


;; SKOOKUM PARSER RULES START


;; TODO finish skooum parser!!!
(defrule skookum-gen-class
         "skookum-class skookum-instance-slot")

(defrule skookum-class
         "#'[A-Z][a-zA-Z]*'")
(defrule skookum-instance-slot
         "#'\\!\\@[a-zA-Z_][a-zA-Z_]*'")


(defparser skookum-blueprint-gen-parser
           [skookum-gen-class-rule
            skookum-class-rule
            skookum-instance-slot-rule])

(insta/parse skookum-blueprint-gen-parser
             "SceneComponent !@default_scene_root")


(insta/parse)


;; SKOOKUM PARSER RULES END








