(ns abhorrent.internal-state-handler-continuations
  (:require [abhorrent.hangman :as hm]
            [abhorrent.watch-directories :as watch])
  (:import (javafx.scene.input KeyCode)
           (org.fxmisc.richtext CodeArea)
           (java_src GenerateEventLoop))
  (:use [abhorrent.state.state]))

(defn- dispatch-char-pressed?
  [key-pressed]
  (let [key (.getCharacter key-pressed)]
    (cond (= "a" key) true
          (= "d" key) true
          ;(= "t" key) true
          :else false)))

(def run hm/run)

(defn- insert-text-clear-event!
  [state code-area text]
  (swap! state assoc :key-typed-event nil)
  (.insertText ^CodeArea code-area (.getCaretPosition code-area) text))

(def consume-keys
  (clojure.string/split "qwertyuiopasdfghjklzxcvbnm -" #""))

;; TODO make dispatching on a certain key easier in case you want to make future modifications
(defn- gen-key-chord
  []
  (hm/coroutine-with-args hm/scope
                          [state key-pressed code-area]
                          (while true
                                 (if (dispatch-char-pressed? key-pressed)
                                     (let [prev-event key-pressed
                                           prev-key (.getCharacter key-pressed)
                                           [timeout? _] (race (do (wait 200) true)
                                                              (do
                                                                (while (= prev-event key-pressed)
                                                                  (yield))
                                                                true))]
                                       (if timeout?
                                         (insert-text-clear-event! state code-area prev-key)
                                         (let [letter (.getCharacter key-pressed)]
                                              (if (= prev-key letter)
                                                  (cond (= letter "a") (insert-text-clear-event! state code-area "(")
                                                        (= letter "d") (insert-text-clear-event! state code-area ")"))
                                                  (insert-text-clear-event! state code-area (str prev-key letter))))))
                                     (insert-text-clear-event! state code-area (.getCharacter key-pressed)))
                                 (yield))))

(defonce trait-key-chord (gen-key-chord))
(defonce multi-method-key-chord (gen-key-chord))

(defonce trait-key-chord-runner (let [loop (GenerateEventLoop/eventLoop
                                             (fn [e]
                                               (let [code-area ^CodeArea (watch/get-trait-code-area)
                                                     ;; TODO might blow up: single event source may conflict with other code area
                                                     event (:key-typed-event @state)]
                                                 (if event
                                                   (run trait-key-chord state event code-area)))))]
                                  (.play loop)
                                  loop))
