(ns abhorrent.hangman
  (:import (java.lang Continuation ContinuationScope)
           (java_src Coroutine MutableData)))

;TODO document and add notes

(defprotocol Run
  (run-0 [this])
  (run-1 [this arg-1])
  (run-2 [this arg-1 arg-2])
  (run-3 [this arg-1 arg-2 arg-3]))

(extend-type Coroutine
  Run
  (run-0 [this]
    (.run this))
  (run-1 [this arg-1]
    (aset (.getArgs this) 0 arg-1)
    (.run this))
  (run-2 [this arg-1 arg-2]
    (aset (.getArgs this) 0 arg-1)
    (aset (.getArgs this) 1 arg-2)
    (.run this))
  (run-3 [this arg-1 arg-2 arg-3]
    (aset (.getArgs this) 0 arg-1)
    (aset (.getArgs this) 1 arg-2)
    (aset (.getArgs this) 2 arg-3)
    (.run this)))

(defn run
  ([this]
   (run-0 this))
  ([this a]
   (run-1 this a))
  ([this a b]
   (run-2 this a b))
  ([this a b c]
   (run-3 this a b c)))

;; TODO possibly remove this macro
(defmacro do-while
  [test & body]
  `(loop []
     ~@body
     (when ~test
       (recur))))

;; place holders for code complete =)
;; these transform in the body of gen-continuation using anaphoric macro expansion

(def ^{:doc
       " yield has two cases...
         yield with no value:
           (yield) => (Continuation/yield scope)
         yield with a value:
           (yield 1) => (do (.setVal coroutine 1) (Continuation/yield scope))
       "}
  yield)

;; this transforms into the current coroutine you are working in
(def ^{:doc
       "transforms into the current coroutine you are working in...
        (cont some-scope (do some things) <this>)
        becomes...
        (cont some-scope (do some things) <cont>)
        referring back to the continuation itself
       "}
  this)

;; this transforms into the current coroutine you are working in
(def ^{:doc
       "transforms into the current coroutine you are working in...
        (cont some-scope (do some things) <this>)
        becomes...
        (cont some-scope (do some things) <cont>)
        referring back to the continuation itself
       "}
  race)

;; this transforms into the current coroutine you are working in
(def ^{:doc
       "transforms into the current coroutine you are working in...
        (cont some-scope (do some things) <this>)
        becomes...
        (cont some-scope (do some things) <cont>)
        referring back to the continuation itself
       "}
  sync)

(def ^{:doc
       "
       "}
  let-mut)

;; wait will wait for a specified amount of time in microseconds until continuing
;; TODO finish wait!
(def wait)

; a default scope to work with
(def scope (ContinuationScope. "default"))



(defn- replace-bind
  [binder args code]
  (let [dist-bindings (map (fn [x] [x (gensym x)]) (filter symbol? (distinct args)))
        binding-replace (reduce (fn [acc [arg sym]]
                                  (assoc acc arg sym))
                                {}
                                dist-bindings)
        replace-all-bindings #(clojure.walk/postwalk (fn [c]
                                                       (if (binding-replace c)
                                                         (binding-replace c)
                                                         c))
                                                     %)
        get-java-mutable #(clojure.walk/postwalk (fn [c]
                                                   (if (binding-replace c)
                                                     `(.getData ~c)
                                                     c))
                                                 %)
        args (reduce (fn [acc [arg val]] (conj acc arg (get-java-mutable val))) [] (partition 2 args))]
    `(~binder ~(replace-all-bindings args)
              ~@(replace-all-bindings code))))

(defn- yield-call?
  [code]
  (and (list? code)
       (or (= (first code) 'yield)
           (= (first code) 'games-with-clojure.hangman/yield))
       (not= (count code) 2)))

(defn- yield-call
  [co scope]
  `(do (.setVal ~co nil)
       (Continuation/yield ~scope)
       nil))

(defn- yield-call-return?
  [code]
  (and (seq? code)
       (or (= (first code) 'yield)
           (= (first code) 'games-with-clojure.hangman/yield))
       (= (count code) 2)))

(defn- yield-call-return
  [co scope code]
  `(do (.setVal ~co ~(last code))
       (Continuation/yield ~scope)
       (.getVal ~co)))

(defn- let-mut?
  [code]
  (and (seq? code)
       (= (first code) 'let-mut)))

(defn- dotimes-call?
  [code]
  (and (seq? code)
       (= (first code) 'dotimes)))

(defn- dotimes-call
  [code]
  (let [[_ args & code] code]
    (replace-bind 'dotimes args code)))

(defn while-call?
  [code]
  (and (seq? code)
       (= (first code) 'while)))

(defn while-call
  [co [_ pred & code]]
  `(while ~pred
     (.setVal ~co nil)
     ~@code))



(defn- let-mut
  [[_ args & code]]
  (let [format (replace-bind 'let-mut args code)
        [_ args & code] format
        groups (partition 2 args)
        vars (into #{} (map first groups))
        args (reduce (fn [acc [var val]]
                       (conj acc var `(MutableData. ~val)))
                     []
                     groups)]
    `(let ~args ~@(clojure.walk/postwalk (fn [c]
                                           (cond (and (seq? c)
                                                      (not= (first c) 'set))
                                                 (map (fn [item]
                                                        (if (contains? vars item)
                                                          `(.getData ~item)
                                                          item))
                                                      c)
                                                 (and (seq? c)
                                                      (= (first c) 'set))
                                                 `(.setData ~(second c) ~(last c))
                                                 :else c))
                                         code))))

(defn wait-call?
  [code]
  (and (seq? code)
       (= (first code) 'wait)
       (= (count code) 1)))

(defn wait-call
  [co scope]
  `(do
     (.setFinishedTime ~co)
     (Continuation/yield ~scope)))

(defn wait-call-duration?
  [code]
  (and (seq? code)
       (= (first code) 'wait)
       (= (count code) 2)))

(defn wait-call-duration
  [co scope [_ duration]]
  `(let [time-to-wait# (+ ~(* duration 1000000) (System/nanoTime))]
     (while (> time-to-wait# (System/nanoTime))
       (.setFinishedTime ~co)
       (Continuation/yield ~scope))))


(defn race-call? [code]
  (and (seq? code)
       (= (first code) 'race)))

(defn sync-call? [code]
  (and (seq? code)
       (= (first code) 'sync)))


(defn- gensymed
  [coroutines]
  (reduce (fn [acc x] (conj acc (gensym) x)) [] coroutines))

(defn- extract-coroutines
  [gensymed]
  (map first (partition 2 gensymed)))

(defn race-sync-transformer
  [co scope code]
  (clojure.walk/postwalk (fn [code]
                           (cond (yield-call-return? code)     (yield-call-return co scope code)
                                 (yield-call? code)            (yield-call co scope)
                                 :else                         code))
                         code))

(defn race-sync-call
  [co race-or-sync scope [_ & code]]
  (let [gensymed (gensymed (map (fn [_] `(Coroutine.)) code))
        coroutines (extract-coroutines gensymed)
        inner-co (gensym)
        transformed (partition 2
                               (interleave coroutines
                                           (map (fn [co code]
                                                  (conj
                                                    `((.setDone ~co true))
                                                    `(.setVal ~co ~(race-sync-transformer co
                                                                                          scope
                                                                                          code))))
                                                coroutines
                                                code)))]
    `(let ~gensymed
       ~@(map (fn [[co code]]
                `(Coroutine/configureCoroutine ~co
                                               ~scope
                                               (fn []
                                                 ~@code)))
              transformed)
       (let [~inner-co (abhorrent.hangman/coroutine ~scope
                                                    (while (do (.setVal ~co
                                                                        ~(mapv (fn [cor] `(first (.run ~cor)))
                                                                               coroutines))
                                                               (~(if (= race-or-sync 'race) 'not-any? 'not-every?)
                                                                 (fn [x#]
                                                                   (.isDone x#))
                                                                 ~(vec coroutines)))
                                                      (Continuation/yield ~scope)))]
         (while (not (and (.run ~inner-co)
                          (.isDone ~inner-co)))
           (Continuation/yield ~scope))
         (mapv (fn [co#] (.getVal co#)) ~(vec coroutines))))))

(defn- co-routine-transformer
  [co scope code]
  (clojure.walk/postwalk (fn [code]
                           (cond (wait-call? code)             (wait-call co scope)
                                 (dotimes-call? code)          (dotimes-call code)
                                 (wait-call-duration? code)    (wait-call-duration co scope code)
                                 (let-mut? code)              (let-mut code)
                                 (yield-call-return? code)     (yield-call-return co scope code)
                                 (yield-call? code)            (yield-call co scope)
                                 (while-call? code)            (while-call co code)
                                 :else                         code))
                         code))

(defn transform-race-sync
  [co scope code]
  (clojure.walk/postwalk (fn [code]
                           (cond (race-call? code)             (race-sync-call co 'race scope code)
                                 (sync-call? code)             (race-sync-call co 'sync scope code)
                                 :else                         code))
                         code))

(defmacro coroutine
  [scope & body]
  (let [co (gensym)
        transformed (co-routine-transformer co scope (transform-race-sync co scope body))]
    `(let [~co (Coroutine.)
           ~(identity 'this) ~co]
       (Coroutine/configureCoroutine ~co
                                     ~scope
                                     (fn []
                                       ~@(concat (butlast transformed)
                                                 `((do (.setVal ~co ~(last transformed))
                                                       (.setDone ~co true))))))
       ~co)))

(defmacro coroutine-with-args
  [scope args & body]
  (let [co (gensym)
        arg-pos   (apply hash-map (interleave args (range (count args))))
        transformed (co-routine-transformer co scope (transform-race-sync co scope body))
        final-transform (clojure.walk/postwalk (fn [x] (if (arg-pos x)
                                                         `(nth (.getArgs ~(identity 'this)) ~(arg-pos x))
                                                         x))
                                               transformed)]
    `(let [~co (Coroutine. (to-array ~(into [] (repeat (count args) nil))))
           ~(identity 'this) ~co]
       (Coroutine/configureCoroutine ~co
                                     ~scope
                                     (fn []
                                       ~@(concat (butlast final-transform)
                                                 `((do (.setVal ~co ~(last final-transform))
                                                       (.setDone ~co true))))))
       ~co)))

;; TEST CODE ;;

(comment
  (do
    (def race-1 (cont scope (println "yo yall") (yield 1) nil))

    (def race-2 (cont scope (dotimes [x 10] (println "hello") (yield x))))
    (def multi
      (race scope
            race-1
            race-2
            (cont scope (println "hello") (yield "third") (yield 4)))))
  (run multi)
  (do
    (def sync-1 (cont scope (println "yo yall") (yield "second")))

    (def sync-2 (cont scope (dotimes [x 5] (println x) (yield x)) "third"))
    (def multi
      (sync scope
            sync-1
            sync-2
            (cont scope "hello" "first"))))

  (run multi))





