(ns abhorrent.windows.creation)

(defn creation-window [{{{:keys [show-creation-window mouse-x mouse-y]} :creation-window-props} :state}]
  {:fx/type       :stage
   :always-on-top true
   :showing       show-creation-window
   :x             mouse-x
   :y             mouse-y
   :width         200
   :height        200
   :style         :undecorated
   :scene         {:fx/type :scene
                   :root    {:fx/type  :h-box
                             :children [{:fx/type :label
                                         :text "Name: "}
                                        {:fx/type :text-field
                                         :on-mouse-exited {:event/type :creation-window-mouse-exit}
                                         :on-action {:event/type :text-field-submitted}
                                         :id "creation-window"}]}}})
