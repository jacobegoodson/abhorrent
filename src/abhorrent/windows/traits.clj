(ns abhorrent.windows.traits
  (:import (javafx.stage Screen))
  (:require [abhorrent.functional-javafx-wrappers :as wrapper]))

(defn traits-window [{{:keys [file-tree-view current-leaf-location ide-windows-viewable trait-context-menus path]} :state}]
  {:fx/type       :stage
   :always-on-top true
   :showing       ide-windows-viewable
   :x             200
   :y             0
   :width         200
   :height        (.getMaxY (.getBounds (Screen/getPrimary)))
   :scene         {:fx/type :scene
                   :root    {:fx/type  :h-box
                             :children [{:fx/type     :tree-view
                                         :on-selected-item-changed {:event/type :trait-selection}
                                         :on-mouse-clicked {:event/type :trait-clicked}
                                         :context-menu {:fx/type :context-menu
                                                        :items trait-context-menus}
                                         :root        (let [leaf (first (filter (fn [{value :value}] (= value "Traits")) file-tree-view))]
                                                        (if leaf
                                                          leaf
                                                          {:fx/type :tree-item
                                                           :value   "Traits"}))}
                                        {:fx/type wrapper/trait-code-area
                                         :id "trait-code-area"
                                         :h-box/hgrow :always}]}}})

