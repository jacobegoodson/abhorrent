(ns abhorrent.windows.project-overview
  (:use [abhorrent.state.state])
  (:require [abhorrent.watch-directories :as watch]
            [abhorrent.functional-javafx-wrappers :as ca]
            [cljfx.api :as fx])
  (:import (javafx.stage Screen)))

(defn project-toggle-group [{:keys [options value on-action current-project]}]
  {:fx/type fx/ext-let-refs
   :refs    {::toggle-group {:fx/type :toggle-group}}
   :desc    {:fx/type :menu-bar
             :menus   [{:fx/type :menu
                        :text    (str "Current Project: " current-project)
                        :items   (into [] (for [option options]
                                            {:fx/type      :radio-menu-item
                                             :toggle-group {:fx/type fx/ext-get-ref
                                                            :ref     ::toggle-group}
                                             :selected     (= option value)
                                             :text         (str option)
                                             :on-action    (assoc on-action :option option)}))}]}})

(defn project-overview-window [{{:keys [current-project file-tree-view]} :state}]
  {:fx/type       :stage
   :always-on-top false
   :showing       true
   :x             0
   :y             0
   :width         200
   :height        (.getMaxY (.getBounds (Screen/getPrimary)))
   :scene         {:fx/type :scene
                   :root    {:fx/type  :v-box
                             :children [{:fx/type         project-toggle-group
                                         :options         (watch/scan-unreal-projects)
                                         :current-project current-project
                                         :on-action       {:event/type :project-selection}}
                                        {:fx/type     :tree-view
                                         :v-box/vgrow :always
                                         :root        {:fx/type  ca/tree-node-file-path
                                                       :value    (if current-project current-project "Select Project")
                                                       :path     (str watch/unreal-projects-dir "\\" current-project)
                                                       :children file-tree-view}}]}}})

;; This will watch the project for changes to the file system
(defn- watching-project [] (watch/project-watcher state))
(watching-project)

;;TODO delete this when done using it as a reference

(comment (defn project-overview [{{:keys [current-project file-tree file-tree-view current-leaf-location]} :state}]
           {:fx/type :stage
            :always-on-top true
            :showing true
            :x 100
            :y 100
            :width 100
            :height 100
            :scene {:fx/type :scene
                    :root    {:fx/type :v-box
                              :children [{:fx/type project-toggle-group
                                          :options (watch/scan-unreal-projects)
                                          :current-project current-project
                                          :on-action {:event/type :project-selection}}
                                         {:fx/type  :h-box
                                          :children [{:fx/type :tree-view
                                                      :on-selected-item-changed {:event/type :tree-item-selected}
                                                      :context-menu {:fx/type :context-menu
                                                                     :items [{:fx/type :menu-item
                                                                              :text ""}
                                                                             {:fx/type :menu-item
                                                                              :text "Delete"
                                                                              :on-action {:event/type :delete-selected
                                                                                          :current-leaf-location current-leaf-location}}]}
                                                      :root    {:fx/type  ca/tree-node-file-path
                                                                :value    (if current-project current-project "Select Project")
                                                                :path     (str watch/unreal-projects-dir "\\" current-project)
                                                                :children file-tree-view}}
                                                     {:fx/type ca/code-area
                                                      :id "code-area"
                                                      :h-box/hgrow :always
                                                      :caret-loc 0}]}]}}}))
