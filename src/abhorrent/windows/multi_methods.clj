(ns abhorrent.windows.multi-methods
  (:require [abhorrent.functional-javafx-wrappers :as ca])
  (:import (javafx.stage Screen)))

(defn multi-methods-window [{{:keys [file-tree-view ide-windows-viewable]} :state}]
  {:fx/type       :stage
   :always-on-top false
   :showing       ide-windows-viewable
   :x             400
   :y             0
   :width         200
   :height        (.getMaxY (.getBounds (Screen/getPrimary)))
   :scene         {:fx/type :scene
                   :root    {:fx/type  :h-box
                             :children [{:fx/type :tree-view
                                         :root    (let [leaf (first (filter (fn [{value :value}] (= value "MultiMethods")) file-tree-view))]
                                                    (if leaf
                                                      leaf
                                                      {:fx/type :tree-item
                                                       :value   "MultiMethods"}))}]}}})

