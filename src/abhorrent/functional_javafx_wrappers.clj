(ns abhorrent.functional-javafx-wrappers
  (:require [cljfx.composite :as co]
            [cljfx.lifecycle :as lifecycle]
            [cljfx.fx.region :as fx.region]
            [cljfx.fx.tree-item :as fx.tree-item])
  (:import (org.fxmisc.richtext CodeArea)
           (java_src TreeNodeFilePath)))

(defn insert-text-at-caret
  [code-area string]
  (.insertText code-area (.getCaretPosition code-area) string))

(def code-area-props (merge fx.region/props
                            (co/props CodeArea
                                      :on-text-changed [:property-change-listener lifecycle/change-listener]
                                      :wrap-text [:setter lifecycle/scalar :default true])))

(def code-area-gen (co/describe CodeArea :ctor [] :props code-area-props))

;;TODO possibly delete this VVV TreeNodeFilePath

(def tree-node-file-path-gen (co/describe TreeNodeFilePath
                                          :ctor []
                                          :props
                                          (merge fx.tree-item/props (co/props TreeNodeFilePath
                                                                              :path [:setter lifecycle/scalar]))))

(defn tree-node-file-path
  [{:keys [path value children]}]
  {:fx/type tree-node-file-path-gen
   :value value
   :path path
   :children children})

(defn trait-code-area
  [{:keys [id path]}]
  {:fx/type         code-area-gen
   :on-text-changed {:event/type :trait-text-changed}
   :event-filter {:event/type :trait-intercept-typing}
   :wrap-text    true
   :id id})

(defn multi-method-code-area
  [{:keys [id]}]
  {:fx/type         code-area-gen
   :on-text-changed {:event/type :multi-method-text-changed}
   :event-filter {:event/type :multi-method-intercept-typing}
   :wrap-text    true
   :id id})

