(ns abhorrent.watch-directories
  (:require [clojure.java.io :as io]
            [watch.man :as watch]
            [instaparse.core :as insta]
            [better-cond.core :as b]
            [abhorrent.functional-javafx-wrappers :as ca])
  (:import (java.nio.file Paths Files FileVisitOption)
           (java.io File)
           (javafx.scene.control TreeItem TreeView ToggleGroup)
           (javafx.scene.layout VBox)
           (javafx.scene Scene)
           (javafx.stage Stage Window)
           (java_src TreeNodeFilePath)
           (javafx.scene.input MouseEvent))
  (:use [abhorrent.hangman]
        ;[abhorrent.javafx-utility]
        [abhorrent.state.state]))

(comment (def current-project "SkiveTestBed")
  (def unreal-projects-dir (str (System/getProperty "user.home") "\\Documents\\Unreal Projects"))
  (def base-directory (str unreal-projects-dir "\\" current-project "\\Scripts"))
  (def blueprint-generated-dir (str base-directory "\\Project-Generated-BP\\"))
  (def skookum-generated-dir   (str base-directory "\\Project\\"))

  ;; this takes the unreal generated bp dir and the skookum generated code and combines them
  (defn extract-directories
    []
    (let [extract #(filter (fn [path] (.isDirectory (io/file path)))
                           (map (fn [path] (.toString path))
                                (-> (File. %)
                                    (.toURI)
                                    (Paths/get)
                                    (Files/walk Integer/MAX_VALUE (into-array FileVisitOption [FileVisitOption/FOLLOW_LINKS]))
                                    (.toArray))))]

      (concat (extract blueprint-generated-dir) (extract skookum-generated-dir))))

  (defn extract-files
    []
    (let [extract #(filter (fn [path] (not (.isDirectory (io/file path))))
                           (map (fn [path] (.toString path))
                                (-> (File. %)
                                    (.toURI)
                                    (Paths/get)
                                    (Files/walk Integer/MAX_VALUE (into-array FileVisitOption [FileVisitOption/FOLLOW_LINKS]))
                                    (.toArray))))]

      (concat (extract blueprint-generated-dir) (extract skookum-generated-dir))))


  (defn read-contents-of-file
    [file]
    (with-open [rdr (clojure.java.io/reader file)]
      (reduce conj [] (line-seq rdr))))

  (defn extract-file-from-path
    [path]
    (last (clojure.string/split path #"\\")))



  ;; TODO extract only the name and type
  ;; TODO extract the class and what it extends as well
  (def test-string "&raw(\"SkySphereMesh\")                     StaticMeshComponent     !@sky_sphere_mesh                    // Sky Sphere Mesh")

  (def paths-being-watched
    (atom #{}))

  (defn delete?
    [types]
    (some #(= :delete %) types))

  (defn create?
    [types]
    (some #(= :create %) types))

  (defn modify?
    [types]
    (some #(= :modify %) types))

  (defn skookum-file?
    [path]
    (= (last (clojure.string/split path #"\.")) "sk"))

  (defn scan-blueprint-defined-classes
    []
    (map #(.toString %)
         (filter #(and (.isFile %) (skookum-file? (.toString %)))
                 (file-seq (io/file blueprint-generated-dir)))))

  (scan-blueprint-defined-classes)

  (defn extract-blueprint-defined-classes
    []
    (reduce #(conj %1
                   (extract-file-from-path %2)
                   (read-contents-of-file %2))
            []
            (scan-blueprint-defined-classes)))

  (defn instance-var-from-blueprint-gen-file?
    [s]
    (= (apply str (take 4 s)) "&raw"))

  (defn extract-blueprint-defined-vars
    [s]
    (if-let [s (re-find #"[a-zA-Z]+[\s]+\!\@[a-zA-Z_]+" s)]
      (into [] (filter #(not= % "") (clojure.string/split s #" ")))))


  ;the slots in skookum come in two flavors: !@<sym> or !@@<sym>
  ;!@ is an instance slot
  ;!@@ is a class slot
  (defn skookum-instance-slot?
    "
    \"!@@dsad\" => false
    \"!@dsad\" => true
    "
    [s]
    (and
      (not= (take 3 s) '(\! \@ \@))
      (= (take 2 s) '(\! \@))))

  (defn skookum-class-slot?
    "
    \"!@@dsad\" => true
    \"!@dsad\" => false
    "
    [s]
    (= (take 3 s) '(\! \@ \@)))


  (defn to-lower-case-add-hyphens
    [s]
    (apply str (concat (clojure.string/lower-case
                         (first s))
                       (map (fn [s]
                              (let [s (str s)]
                                (if (re-find #"[A-Z]" s)
                                  (str "-" (clojure.string/lower-case s))
                                  s)))
                            (rest s)))))


  (defn skookum-class->abhorrent-class
    "\"SceneComponent\" => \"scene-component!\""
    [s]
    (apply str (conj (map (fn [s]
                            (let [s (str s)]
                              (if (re-find #"[A-Z]" s)
                                (str "-" (clojure.string/lower-case s))
                                s)))
                          (concat (rest s) "!"))
                     (clojure.string/lower-case (first s)))))

  (defn skookum-anyclass->abhorrent-class
    [s]
    (insta/transform
      {:cap-standard-class (fn [x] (str "-" (to-lower-case-add-hyphens x)))
      :cap-class (fn [x & args] (apply str (concat x args)))
      :underscore (fn [_] "-")
      :standard-class (fn [cl] (to-lower-case-add-hyphens cl))
      :skookum-class (fn [& args] (str (apply str args) "!"))
      :enum (fn [e & args] (str e "-" (apply str args)))
      :fbx (fn [fbx & args] (str fbx "-" (apply str args)))
      :num identity}
      (insta/parse
        (insta/parser
          "
          skookum-class = (fbx / enum / standard-class / cap-class / underscore / num)+
          fbx = 'FBX'
          enum = 'E'
          cap-class = #'[A-Z][A-Z]+' [cap-standard-class]
          cap-standard-class = #'([a-z]+|([A-Z][a-z]+))+'
          underscore = '_'
          standard-class = #'([A-Z][a-z]+)+'
          num = #'[0-9]+'
          ")
        s)))

  (defn convert-skookum-class-defined-file
    "\"StandardMacros.Entity.sk\" => {:name \"Entity\", :extends \"StandardMacros\"}"
    [sk-file-name]
    (let [[extends obj _] (clojure.string/split sk-file-name #"\.")]
      {:name (skookum-anyclass->abhorrent-class obj)
      :extends (skookum-anyclass->abhorrent-class extends)
      :class-slots []
      :instance-slots []}))

  (defn gather-class-information
    [[{:keys [name extends class-slots instance-slots]} & slots]]
    (reduce
      (fn [acc slot]
        (if (instance? instance-slot slot)
          (update acc :instance-slots conj slot)
          (update acc :class-slots conj slot)))
      (skookum-generated-class. name extends class-slots instance-slots)
      slots))


  (defn skookum-slot->abhorrent-slot
    "
    \"!@hello_there\" => \"hello-there\"
    \"!@@hello_there\" => \"hello-there\"
    "
    [s]
    (->> (-> (clojure.string/split s #"(\![\@]+)|_")
             rest)
         (interpose "-")
         (apply str)))
  (defrecord skookum-generated-class [name extends class-slots instance-slots])
  (defrecord class-slot [type slot])
  (defrecord instance-slot [type slot])

  (defn classify-skookum-slot
    [[type slot]]
    (let [ab-class (skookum-anyclass->abhorrent-class type)
         ab-slot (skookum-slot->abhorrent-slot slot)]
      (if (skookum-class-slot? slot)
        (class-slot. ab-class ab-slot)
        (instance-slot. ab-class ab-slot))))

  (defn classify-skookum-slots
    [slots]
    (map classify-skookum-slot slots))



  (defn extract-blueprint-generated-classes
    [sk-classes]
    (map gather-class-information
      (map (fn [[c code]] (let [vars (filter instance-var-from-blueprint-gen-file? code)]
                            (if (empty? vars)
                              [(convert-skookum-class-defined-file c)]
                              `[~(convert-skookum-class-defined-file c) ~@(classify-skookum-slots (map extract-blueprint-defined-vars vars))])))
           (partition 2 sk-classes))))

  (extract-blueprint-generated-classes (extract-blueprint-defined-classes)))

;;TODO the code above is old, needs another look and some refactoring, contains useful parsing code
;; TODO for skookum

(def unreal-projects-dir (str (System/getProperty "user.home") "\\Documents\\Unreal Projects"))
(defn scan-unreal-projects
  []
  (filterv #(not (nil? %))
           (map #(let [last (last (clojure.string/split (.toString %) #"\\"))]
                   (if (not (re-find #"Intermediate" last))
                     last))
                (.listFiles (io/file unreal-projects-dir)))))

(defn children
  [dir]
  (if (.isDirectory dir)
    (into [] (.listFiles dir))
    []))

(defrecord file [ref name location])
(defrecord folder [ref name location contents])

(defn base-object-location
  [project-name]
  (str unreal-projects-dir "\\" project-name "\\Scripts\\Project\\Object"))

(defn- inner-file-tree
  [dir]
  (reduce (fn [acc nex]
            (if (not (empty? (children nex)))
              (conj acc (folder. (io/file (.getAbsolutePath nex))
                                 (.getName nex)
                                 (.getAbsolutePath nex)
                                 (inner-file-tree (children nex))))
              (if (.isDirectory nex)
                (conj acc (folder. (io/file (.getAbsolutePath nex)) (.getName nex) (.getAbsolutePath nex) {}))
                (conj acc (file. (io/file (.getAbsolutePath nex)) (.getName nex) (.getAbsolutePath nex))))))
          []
          dir))

(defn file-tree
  [dir]
  (inner-file-tree (into []
                         (-> (base-object-location dir)
                             (io/file)
                             (.getParentFile)
                             (.listFiles)))))

;; TODO transform folders and files visual rep into skive/abhorent counter parts
(defn transform-folder-contents-into-tree-leaves
  [folder-tree]
  (clojure.walk/postwalk (fn [x]
                           (if (record? x)
                             (let [{:keys [name location contents]} x]
                               (if contents
                                 (if-not (empty? contents)
                                   {:fx/type  ca/tree-node-file-path
                                    :value    name
                                    :path     location
                                    :children contents}
                                   {:fx/type ca/tree-node-file-path
                                    :path location
                                    :value   name})
                                 {:fx/type ca/tree-node-file-path
                                  :path location
                                  :value   name}))
                             x))
                         folder-tree))

(defn infinite-loop [function]
  (function)
  (future (infinite-loop function))
  nil)

(defonce project-watcher
         (fn [state]
           (infinite-loop #(do (Thread/sleep 200)
                               (let [file-tree (file-tree (:current-project @state))
                                     file-tree-view (transform-folder-contents-into-tree-leaves file-tree)]
                                 (swap! state
                                        assoc
                                        :file-tree file-tree
                                        :file-tree-view file-tree-view))))))

(defn get-code-area
  []
  (.lookup (.getScene (first (Window/getWindows))) "#code-area"))
;;TODO may cause errors looking for positional keys, change this to search in future
(defn get-traits
  [state]
  (last (:file-tree @state)))

(defn get-node-by-id
  [id]
  (first (filter #(not (nil? %))
                 (map #(.lookup (.getScene %) (str "#" id))
                      (Window/getWindows)))))

(defn get-trait-code-area
  []
  (get-node-by-id "trait-code-area"))

(defn get-trait-directory
  []
  (io/file (:path (first (filter (fn [{value :value}] (= value "Traits")) (@state :file-tree-view))))))

(defn get-methods
  [state]
  (first (:file-tree @state)))





