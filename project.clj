(defproject abhorrent "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.fxmisc.richtext/richtextfx "0.10.4"]
                 [instaparse "1.4.10"]
                 [parinferish "0.7.0"]
                 [spootnik/watchman "0.3.7"]
                 [better-cond "2.1.0"]
                 [cljfx "1.6.8"]
                 [org.clojure/core.cache "1.0.207"]]
  :java-source-paths ["src"]
  :repl-options {:init-ns abhorrent.core})
;:repl-options {:init-ns abhorrent.core})
